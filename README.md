public class Variables {
    static byte myByte;
    static short myShort;
    static int myInt;
    static long myLong;
    static float myFloat;
    static double myDouble;
    static char myChar;
    static boolean myBoolean;
    static String myString;

    byte myByte1;
    short myShort1;
    int myInt1;
    long myLong1;
    float myFloat1;
    double myDouble1;
    char myChar1;
    boolean myBoolean1;
    String myString1;

    public static void main(String[] args) {


        System.out.println("Статическая переменная byte инициализируется по умолчанию ..." + myByte);
        System.out.println("Статическая переменная short инициализируется по умолчанию ..." + myShort);
        System.out.println("Статическая переменная int инициализируется по умолчанию ..." + myInt);
        System.out.println("Статическая переменная long инициализируется по умолчанию ..." + myLong);
        System.out.println("Статическая переменная float инициализируется по умолчанию ..." + myFloat);
        System.out.println("Статическая переменная double инициализируется по умолчанию ..." + myDouble);
        System.out.println("Статическая переменная char инициализируется по умолчанию ..." + myChar);
        System.out.println("Статическая переменная boolean инициализируется по умолчанию ..." + myBoolean);
        System.out.println("Статическая переменная String инициализируется по умолчанию ..." + myString);

        System.out.println();

        Variables object = new Variables();

        System.out.println("Нестатическая (объектная) переменная byte инициализируется по умолчанию ..." + object.myByte1);
        System.out.println("Нестатическая (объектная) переменная short инициализируется по умолчанию ..." + object.myShort1);
        System.out.println("Нестатическая (объектная) переменная int инициализируется по умолчанию ..." + object.myInt1);
        System.out.println("Нестатическая (объектная) переменная long инициализируется по умолчанию ..." + object.myLong1);
        System.out.println("Нестатическая (объектная) переменная float инициализируется по умолчанию ..." + object.myFloat1);
        System.out.println("Нестатическая (объектная) переменная double инициализируется по умолчанию ..." + object.myDouble1);
        System.out.println("Нестатическая (объектная) переменная char инициализируется по умолчанию ..." + object.myChar1);
        System.out.println("Нестатическая (объектная) переменная boolean инициализируется по умолчанию ..." + object.myBoolean1);
        System.out.println("Нестатическая (объектная) переменная String инициализируется по умолчанию ..." + object.myString1);
    }


}
