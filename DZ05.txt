public class Minimum {
    /**
     * Предположим, дана последовательность чисел 244, 169, 772, -1.
     * Реализуем программу, которая для последовательности чисел,
     * оканчивающихся на -1 выведет самую минимальную цифру,
     * встречающуюся среди чисел последовательности.
     * Для этой цели создаём массив из последовательности цифр,
     * входящих в вышеуказанные числа.
     */

    public static void main(String[] args) {
        int[] numbers = {2, 4, 4, 1, 6, 9, 7, 7, 2};
        int min = getMin(numbers);
        System.out.println("Минимальное значение равняется: " + min);
        System.out.println("Изменения для пушинга");
    }

    public static int getMin(int[] numbers) {
        int minValue = numbers[0];
        for (int i = 1; i < numbers.length; i++) {
            if (numbers[i] < minValue) {
                minValue = numbers[i];
            }
        }
        return minValue;

    }
}

