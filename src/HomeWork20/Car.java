package HomeWork20;

public class Car {
    private String id;
    private String brand;
    private String color;
    private int carRun;
    private int price;

    public Car(String id, String brand, String color, int carRun, int price) {
        this.id = id;
        this.brand = brand;
        this.color = color;
        this.carRun = carRun;
        this.price = price;
    }

    public String getId() {
        return id;
    }

    public String getBrand() {
        return brand;
    }

    public String getColor() {
        return color;
    }

    public int getCarRun() {
        return carRun;
    }

    public int getPrice() {
        return price;
    }

    @Override
    public String toString() {
        return "Car{" +
                "id='" + id + '\'' +
                ", brand='" + brand + '\'' +
                ", color='" + color + '\'' +
                ", carRun=" + carRun +
                ", price=" + price +
                '}';
    }
}
