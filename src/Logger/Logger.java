package Logger;

public class Logger {
    private static Logger logger;
    private static String message = "Первое лог - сообщение";

    public static Logger getLogger() {
        if (logger == null) {
            logger = new Logger();
        }
        return logger;
    }

    private Logger() {

    }


    public void showMassage() {
        System.out.println(message);
    }
}

