package Figure;

public class Circle extends Ellipse implements CanMove {
    public Circle(double x) {
        super(x, x);
    }

    @Override
    public double getArea() {
        return 3.14 * getX() * getX();
    }

    @Override
    public double getPerimetr() {
        return 2 * 3.14 * getX();
    }

    @Override
    public void getShift() {
        double x = getX();

        System.out.println("Kоординаты круга: " + "х = " + getX() + "," + " у = " + getY());
    }
}
