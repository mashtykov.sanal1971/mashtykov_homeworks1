package Figure;

public abstract class Figure {
    private double x;
    private double y;

    public Figure(double x, double y) {
        this.x = x;
        this.y = y;
    }
    public abstract double getArea();

    public abstract double getPerimetr();

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }
}
