package Figure;

public class Ellipse extends Figure{
    public Ellipse(double x, double y) {
        super(x, y);
    }

    @Override
    public double getPerimetr() {
        double z = Math.sqrt((getX()*getX() + getY()*getY())/8);
        return 2 *3.14 * z;
    }

    @Override
    public double getArea() {
        return 2 * 3.14 * getX() * getY();


    }
}
