package HomeWork13;

import HomeWork13.ByCondition;

public class Sequence {
            public static int[] filter(int[] array, ByCondition condition){
            int[] result = new int[12];
            for (int i : array){
                if (condition.isOk(i)){
                    result[i] = i;
                    i++;
                }
            }
            return result;
        }

    }
