import java.util.Arrays;

public class Human {
    private String name;
    private double weight;

    public Human(String name, double weight) {
        this.name = name;
        this.weight = weight;
    }

    public String getName() {
        return name;
    }

    public double getWeight() {
        return weight;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }
    static Human[] human = new Human[10];
    public static void main(String[] args) {
                human [0] = new Human ("Владимир", 80.1);
        human [1] = new Human("Галина", 60.7);
        human [2] = new Human ("Сергей", 93.0);
        human [3] = new Human("Милена", 58.8);
        human [4] = new Human("Батр",90.2  );
        human [5] = new Human("Борис", 87.0);
        human [6] = new Human("Валерий",90.3  );
        human [7] = new Human("Амуланга", 57.3);
        human [8] = new Human("Мерген", 72.5);
        human [9] = new Human("Иляна",48.7);

        selectionSort(human);
        for (Human human : human) {
            System.out.println(human.getName() + "  " + human.getWeight() + "  ");
        }
    }
    public static void selectionSort(Human[] humans){
        for (int i = 0; i < humans.length - 1; i++) {
            int minIndex = i;
            for (int j = i; j < humans.length; j++) {
                if (human[j].getWeight() < human[minIndex].getWeight()) {
                    minIndex = j;
                }
            }

            Human temp = human[i];
            human[i] = human[minIndex];
            human[minIndex] = temp;
        }
    }

}












//        Double [] human = new Double[10];
//        human[0]=human1.getWeight();
//        human[1]= human2.getWeight();;
//        human[2]=human3.getWeight();
//        human[3]=human4.getWeight();
//        human[4]=human5.getWeight();
//        human[5]=human6.getWeight();
//        human[6]=human7.getWeight();
//        human[7]=human8.getWeight();
//        human[8]=human9.getWeight();
//        human[9]=human10.getWeight();

        //System.out.println(Arrays.toString(human));


