package main.java.com.homeworks.homework16;

public class ArrayList<T> {
            private static final int DEFAULT_SIZE = 10;
        private  T[]elements;
        private int size;

        public ArrayList(){
            this.elements =(T[]) new Object[DEFAULT_SIZE];
            this.size = 0;
        }
        public void add(T element){
            if(size < elements.length){
                this.elements[size] = element;
                size++;
            }
            else{
                System.err.println("spisok perepolnen");
            }

        }
        public boolean removeAt(int index) {
            checkIndex(index);
            for (int i = index; i < size - 1; i++) {
                elements[i] = elements[i + 1];
            }
            size--;
            return true;
        }
        private void checkIndex(int index){
            if (index<0 || index>size){
                throw new IndexOutOfBoundsException();
            }
        }
        public T get(int index){
            if (index >= 0 && index < size){
                return elements[index];
            }
            else {
                return null;
            }
        }
    }


