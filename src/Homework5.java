public class Homework5 {
    /**Предположим, дана последовательность чисел 244, 169, 772, -1.
     Реализуем программу, которая для последовательности чисел,
     оканчивающихся на -1 выведет самую минимальную цифру,
     встречающуюся среди чисел последовательности.
     Для этой цели создаём массив из последовательности цифр,
     входящих в вышеуказанные числа.*/
           public static void searchMin(int[] numbers){
            for (int i = 0;i<numbers.length;i++){
                int min = numbers[i];
                int minIndex = i;
                for (int j = i;j<numbers.length;j++){
                    if (numbers[j]<min){
                        min = numbers[j];
                        minIndex = j;
                    }
                }
                int temp = numbers[i];
                numbers[i] = numbers[minIndex];
                numbers[minIndex] = temp;



                System.out.println(numbers[0]);
                break;
            }
        }
        public static void main(String[] args) {
            int[] numbers = {2,4,4,1,6,9,7,7,2};
//        int[] numbers = new int[9];
//        numbers[0] = 2;
//        numbers[1] = 4;
//        numbers[2] = 4;
//        numbers[3] = 1;
//        numbers[4] = 6;
//        numbers[5] = 9;
//        numbers[6] = 7;
//        numbers[7] = 7;
//        numbers[8] = 2;

            searchMin(numbers);


        }
    }

