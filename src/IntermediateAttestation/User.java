package IntermediateAttestation;

public class User {
    private int id;
    private String name;
    private int age;
    private boolean worker;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public boolean isWorker() {
        return worker;
    }

    public void setWorker(boolean worker) {
        this.worker = worker;
    }

    public User(int id, String name, int age, boolean worker) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.worker = worker;
    }

    @Override
    public String toString() {
        return id + "|" + name + "|" + age + "|" + worker;
    }
}


