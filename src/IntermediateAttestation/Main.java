package IntermediateAttestation;

public class Main {
    public static void main(String[] args) {
        UserRepository usersRepository = new UsersRepositoryFileImpl("user.txt");
        User user = usersRepository.findById(1);
        System.out.println(usersRepository.findById(1));
        user.setName("Марсель");
        user.setAge(27);
        usersRepository.update(user);
        System.out.println(usersRepository.findById(1));
    }
}