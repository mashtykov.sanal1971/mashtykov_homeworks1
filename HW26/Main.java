import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.activation.DataSource;

public class Main {
            public static void main(String[] args) {
            DriverManagerDataSource dataSource = new DriverManagerDataSource(
                    "jdbc:postgresql://localhost:5432/homework_25",
                    "postgres",
                    "1");

            ShopClothingServices usersRepository = new ProductRepository(dataSource);

            Product product = Product.builder()
                    .brand("Nike")
                    .typeClothing("Sweatshirt")
                    .gender("Male")
                    .price(3.399)
                    .count(14)
                    .build();
            usersRepository.save(product);

            System.out.println(usersRepository.findAll());
            System.out.println(usersRepository.findAllEqualsByPrice(2.499));
            System.out.println(usersRepository.findAllGreaterByPrice(4.000));
            System.out.println(usersRepository.findAllLessByPrice(3.000));
        }
    }


