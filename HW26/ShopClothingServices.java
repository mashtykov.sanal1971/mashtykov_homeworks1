import java.util.List;

public interface ShopClothingServices {

        List<Product> findAll();

        void save(Product product);

        List<Product> findAllEqualsByPrice(double price);

        List<Product> findAllGreaterByPrice(double price);

        List<Product> findAllLessByPrice(double price);
    }


