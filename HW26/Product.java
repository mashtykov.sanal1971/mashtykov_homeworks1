import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Product {


        private int id;
        private String brand;
        private String typeClothing;
        private String gender;
        private double price;
        private int count;
    }



