import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.util.List;

public class ProductRepository implements ShopClothingServices{

            private static final SqlCommand sqlCommand = new SqlCommand();

        private final JdbcTemplate jdbcTemplate;

        private static final RowMapper<Product> productRowMapper = (row, number) -> {
            int id = row.getInt("id");
            String brand = row.getString("brand");
            String typeClothing = row.getString("type_clothing");
            String gender = row.getString("gender");
            double price = row.getDouble("price");
            int count = row.getInt("count");

            return new Product(id, brand, typeClothing, gender, price, count);
        };

        public ProductRepository(DataSource dataSource) {
            this.jdbcTemplate = new JdbcTemplate(dataSource);
        }

        @Override
        public List<Product> findAll() {
            return jdbcTemplate.query(sqlCommand.getSqlSelectAll(), productRowMapper);
        }

        @Override
        public void save(Product product) {
            jdbcTemplate.update(sqlCommand.getSqlInsert(),
                    product.getBrand(),
                    product.getTypeClothing(),
                    product.getGender(),
                    product.getPrice(),
                    product.getCount());
        }

        @Override
        public List<Product> findAllEqualsByPrice(double price) {
            return jdbcTemplate
                    .query(sqlCommand.getSqlSelectAllByPrice() +
                            price, productRowMapper);
        }

        @Override
        public List<Product> findAllGreaterByPrice(double price) {
            return jdbcTemplate
                    .query(sqlCommand.getSqlSelectGreaterByPrice() +
                            price, productRowMapper);
        }

        @Override
        public List<Product> findAllLessByPrice(double price) {
            return jdbcTemplate
                    .query(sqlCommand.getSqlSelectLessByPrice() +
                            price, productRowMapper);
        }
    }

    class SqlCommand {
        private static final String SQL_INSERT =
                "INSERT INTO products(brand, type_clothing, gender, price, count) " +
                        "VALUES (?, ?, ?, ?, ?)";

        private static final String SQL_SELECT_ALL =
                "SELECT * FROM products ORDER BY id";
        private static final String SQL_SELECT_ALL_BY_PRICE =
                "SELECT * FROM products WHERE price = ";
        private static final String SQL_SELECT_GREATER_BY_PRICE =
                "SELECT * FROM products WHERE price > ";
        private static final String SQL_SELECT_LESS_BY_PRICE =
                "SELECT * FROM products WHERE price < ";

        public String getSqlInsert() {
            return SQL_INSERT;
        }

        public String getSqlSelectAll() {
            return SQL_SELECT_ALL;
        }

        public String getSqlSelectAllByPrice() {
            return SQL_SELECT_ALL_BY_PRICE;
        }

        public String getSqlSelectGreaterByPrice() {
            return SQL_SELECT_GREATER_BY_PRICE;
        }

        public String getSqlSelectLessByPrice() {
            return SQL_SELECT_LESS_BY_PRICE;
        }
    }


