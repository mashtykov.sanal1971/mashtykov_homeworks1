CREATE TABLE products
(
    id            SERIAL PRIMARY KEY,
    brand         VARCHAR(20),
    type_clothing VARCHAR(20),
    gender        VARCHAR(10),
    price         DOUBLE PRECISION,
    count         INT CHECK (count > 0)
);

CREATE TABLE clients
(
    id         SERIAL PRIMARY KEY,
    first_name VARCHAR(20),
    last_name  VARCHAR(20),
    address    VARCHAR(100)
);

CREATE TABLE orders
(
    id            SERIAL PRIMARY KEY,
    product_id    INT,
    client_id     INT,
    FOREIGN KEY (product_id) REFERENCES products (id),
    FOREIGN KEY (client_id) REFERENCES clients (id),
    product_total INT,
    oder_date     DATE NOT NULL,
    delivery_date DATE NOT NULL
);
