<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<body>


<h2>All Owners</h2>
<br>

<table>

    <tr>
        <th>FullName</th>
        <th>
        <th>
        <th>
        <th>PhoneNumber</th>
        <th>
        <th>
        <th>
        <th>KindOfAnimal</th>
        <th>
        <th>
        <th>
        <th>SexOfAnimal</th>
        <th>
        <th>
        <th>
        <th>BreedOfAnimal</th>
        <th>
        <th>
        <th>
        <th>AgeOfAnimal</th>
        <th>
        <th>
        <th>
        <th>ReasonOfVisit</th>
    </tr>

    <c:forEach var="own" items="${allOwns}">

        <tr>
            <td>${own.fullName}</td>
            <td>${own.phoneNumber}</td>
            <td>${own.kindOfAnimal}</td>
            <td>${own.sexOfAnimal}</td>
            <td>${own.breedOfAnimal}</td>
            <td>${own.ageOfAnimal}</td>
            <td>${own.reasonOfVisit}</td>
        </tr>
    </c:forEach>

</table>

</body>







</html>