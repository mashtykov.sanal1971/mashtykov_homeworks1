package com.Innopolis;

public class NumbersUtil {
    private int number;

    public int gcd(int a, int b) {

        if (b == 0) {
            return a;
        } else {
            return gcd(b, a % b);
        }
    }
        public boolean isGcd(int number) {
            if (number == 0 || number == 1) {
                throw new IllegalArgumentException();
            }

            if (number == 2 || number == 3) {
                return false;
            }

            return true;
        }


}
