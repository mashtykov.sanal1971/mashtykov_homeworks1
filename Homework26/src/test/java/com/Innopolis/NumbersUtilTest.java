package com.Innopolis;

import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.*;


@DisplayName(value = "NumbersUtil is working when")
@DisplayNameGeneration(value = DisplayNameGenerator.ReplaceUnderscores.class)
class NumbersUtilTest {

    private final NumbersUtil numbersUtil = new NumbersUtil();



    @Nested
    public class Forgcd {

        @ParameterizedTest(name = "return <true> on {0}")
        @ValueSource(ints = {18, 12, 6})
        public void on_Gcd_numbers_return_true(int number) {
            assertTrue(numbersUtil.isGcd(number));
        }

        @ParameterizedTest(name = "return <false> on {0}")
        @ValueSource(ints = { 1, 2, 3})
        public void on_not_Gcd_numbers_return_false(int number) {
            assertFalse(numbersUtil.isGcd(number));
        }

        @ParameterizedTest(name = "throws exception on {0}")
        @ValueSource(ints = {0})
        public void bad_numbers_throwsGcd_exception(int badNumber) {
            assertThrows(IllegalArgumentException.class, () -> numbersUtil.isGcd(badNumber));
        }

        @Nested
        @DisplayName("gpt() is working")
        public class ForGcd {
            @ParameterizedTest(name = "return {2} on {0} + {1}")
            @CsvSource(value = {"18, 12, 6", "10, 2, 2", "64, 48, 16"})
            public void return_correct_gcd(int a, int b, int result) {
                assertEquals(result, numbersUtil.gcd(a, b));
            }
        }
   }
}