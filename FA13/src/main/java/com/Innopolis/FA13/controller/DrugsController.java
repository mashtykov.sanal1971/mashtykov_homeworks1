package com.Innopolis.FA13.controller;

import com.Innopolis.FA13.forms.DrugForm;
import com.Innopolis.FA13.models.Drug;
import com.Innopolis.FA13.models.Purchase;
import com.Innopolis.FA13.services.DrugsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.util.List;

@Controller
public class DrugsController {

    private final DrugsService drugsService;

    @Autowired
    public DrugsController(DrugsService drugsService) {
        this.drugsService = drugsService;
    }

    @GetMapping("/drugs")
    public String getDrugsPage(Model model) {
        List<Drug> drugs = drugsService.getAllDrugs();
        model.addAttribute("drugs", drugs);
        return "drugs";
    }

    @GetMapping("/drugs/{drug-id}")
    public String getDrugPage(Model model, @PathVariable("drug-id") Integer drugId) {
        Drug drug = drugsService.getDrug(drugId);
        model.addAttribute("drug", drug);
        return "drug";
    }
    @PostMapping("/drugs")
    public String addDrug(@Valid DrugForm form) {
        drugsService.addDrug(form);
        return "redirect:/drugs";
    }

    @PostMapping("/drugs/{drug-id}/delete")
    public String deleteDrug(@PathVariable("drug-id") Integer drugId) {
        drugsService.deleteDrug(drugId);
        return "redirect:/drugs";
    }
    @PostMapping("/drugs/{drug-id}/update")
    public String update(@Valid DrugForm form, @PathVariable("drug-id") Integer drugId) {
        drugsService.update(form, drugId);
        return "redirect:/drugs";
    }

    @GetMapping("/drugs/{drug-id}/purchases")
    public String getPurchasesByDrug(Model model, @PathVariable("drug-id") Integer drugId) {
        List<Purchase> purchases = drugsService.getPurchasesByDrug(drugId);
        model.addAttribute("purchases", purchases);
        return "purchases_of_drug";
    }

    @PostMapping("/drugs/{drug-id}/purchases")
    public String addPurchaseToDrug(@PathVariable("drug-id") Integer drugId, @RequestParam("purchaseId") Integer purchaseId) {
        drugsService.addPurchaseToDrug(drugId, purchaseId);
        return "redirect:/drugs/" + drugId + "/purchases";
    }



}

