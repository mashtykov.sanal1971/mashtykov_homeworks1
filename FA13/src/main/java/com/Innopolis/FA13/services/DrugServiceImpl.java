package com.Innopolis.FA13.services;

import com.Innopolis.FA13.forms.DrugForm;
import com.Innopolis.FA13.models.Drug;
import com.Innopolis.FA13.models.Purchase;
import com.Innopolis.FA13.repositories.DrugsRepository;
import com.Innopolis.FA13.repositories.PurchaseRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
;

import java.util.List;

@RequiredArgsConstructor
@Component
public class DrugServiceImpl implements DrugsService {

    private final DrugsRepository drugsRepository;
    private final PurchaseRepository purchasesRepository;

    @Override
    public void addDrug(DrugForm form) {
        Drug drug = Drug.builder()
                .description(form.getDescription())
                .price(form.getPrice())
                .quantity(form.getQuantity())
                .build();

        drugsRepository.save(drug);
    }

    @Override
    public List<Drug> getAllDrugs() {
        return drugsRepository.findAll();
    }

    @Override
    public void deleteDrug(Integer drugId) {
        drugsRepository.deleteById(drugId);
    }

    @Override
    public Drug getDrug(Integer drugId) {
        return drugsRepository.getById(drugId);
    }

    @Override
    public List<Purchase> getPurchasesByDrug(Integer drugId) {
        return purchasesRepository.findAllByOwner_Id(drugId);
    }

    @Override
    public void addPurchaseToDrug(Integer drugId, Integer purchaseId) {
        Drug drug = drugsRepository.getById(drugId);
        Purchase purchase = purchasesRepository.getById(purchaseId);
        purchase.setOwner(drug);
        purchasesRepository.save(purchase);
    }

    @Override
    public void update(DrugForm form, Integer drugId) {
        Drug drug = Drug.builder()
                .id(drugId)
                .description(form.getDescription())
                .price(form.getPrice())
                .quantity(form.getQuantity())
                .build();

        drugsRepository.save(drug);
    }
}
