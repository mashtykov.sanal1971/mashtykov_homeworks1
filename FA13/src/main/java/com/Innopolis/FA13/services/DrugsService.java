package com.Innopolis.FA13.services;

import com.Innopolis.FA13.forms.DrugForm;
import com.Innopolis.FA13.models.Drug;
import com.Innopolis.FA13.models.Purchase;

import java.util.List;


public interface DrugsService {
    void addDrug(DrugForm form);
    List<Drug> getAllDrugs();

    void deleteDrug(Integer DrugId);

    Drug getDrug(Integer drugId);

    List<Purchase> getPurchasesByDrug(Integer drugId);

    void addPurchaseToDrug(Integer drugId, Integer purchasesId);

    void update(DrugForm form, Integer drugId);
}

