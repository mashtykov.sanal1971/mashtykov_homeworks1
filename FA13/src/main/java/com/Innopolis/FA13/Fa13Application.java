package com.Innopolis.FA13;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Fa13Application {

	public static void main(String[] args) {
		SpringApplication.run(Fa13Application.class, args);
	}

}
