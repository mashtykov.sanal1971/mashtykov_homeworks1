package com.Innopolis.FA13.forms;

import lombok.Data;

@Data
public class DrugForm {
    private String description;
    private Integer price;
    private Integer quantity;
}
