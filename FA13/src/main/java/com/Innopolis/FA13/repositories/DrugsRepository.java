package com.Innopolis.FA13.repositories;


import com.Innopolis.FA13.models.Drug;
import org.springframework.data.jpa.repository.JpaRepository;


public interface DrugsRepository extends JpaRepository<Drug, Integer> {

}

