package com.Innopolis.FA13.repositories;

import com.Innopolis.FA13.models.Purchase;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PurchaseRepository extends JpaRepository<Purchase, Integer> {
    List<Purchase> findAllByOwner_Id(Integer id);
    List<Purchase> findAllByOwnerIsNull();

}