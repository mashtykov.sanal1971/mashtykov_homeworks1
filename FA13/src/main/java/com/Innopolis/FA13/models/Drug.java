package com.Innopolis.FA13.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
public class Drug {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    private Integer id;
    private String description;
    private Integer price;
    private Integer quantity;

    @OneToMany(mappedBy = "owner")
    private List<Purchase> purchases;
}
