create table drug
(
    id          serial primary key,
    description varchar(20),
    price       DECIMAL(15, 2),
    quantity    Integer
);


create table customer
(
    id   serial primary key,
    name varchar(50)
);

create table purchases
(
    id              serial primary key,
    owner_id        integer,
    foreign key (owner_id) references drug (id),
    owner1_id       integer,
    foreign key (owner1_id) references customer (id),
    dateOfPurchase  date,
    quantityOfDrugs Integer
);


insert into drug (description, price, quantity)
VALUES ('Гепасейф', 283.50, 200);
insert into drug (description, price, quantity)
VALUES ('Тилозин', 142.00, 150);
insert into drug (description, price, quantity)
VALUES ('Ципровет', 167.00, 100);
insert into drug (description, price, quantity)
VALUES ('Синулокс', 146.50, 100);
insert into drug (description, price, quantity)
VALUES ('Рикарфа', 511.50, 105);
insert into drug (description, price, quantity)
VALUES ('Пиро-Стоп', 826, 500);
insert into drug (description, price, quantity)
VALUES ('Террамицин', 571.00, 250);
insert into drug (description, price, quantity)
VALUES ('Трококсил', 2037.00, 45);


update drug
set price   = 152,
    quantity=130
where description = 'Гепасейф'
  and id = 1;


insert into customer (name)
values ('Бадмаев А');
insert into customer (name)
values ('Манджиев К');
insert into customer (name)
values ('Лиджиева Э');
insert into customer (name)
values ('Французов О');
insert into customer (name)
values ('Генджиев А');


insert into purchases (nomenklatura_id, customer_id, dateOfPurchase, quantityOfDrugs)
VALUES (1, 1, '10.09.2021', 3);
insert into purchases (nomenklatura_id, customer_id, dateOfPurchase, quantityOfDrugs)
VALUES (5, 2, '13.09.2021', 2);
insert into purchases (nomenklatura_id, customer_id, dateOfPurchase, quantityOfDrugs)
VALUES (2, 3, '15.09.2021', 4);
insert into purchases (nomenklatura_id, customer_id, dateOfPurchase, quantityOfDrugs)
VALUES (4, 5, '16.09.2021', 1);
insert into purchases (nomenklatura_id, customer_id, dateOfPurchase, quantityOfDrugs)
VALUES (8, 4, '20.09.2021', 2);
insert into purchases (nomenklatura_id, customer_id, dateOfPurchase, quantityOfDrugs)
VALUES (6, 5, '23.09.2021', 3);


select *
from purchases
order by purchases.dateOfPurchase;


select a.name, b.dateOfPurchase, b.quantityOfDrugs
from customer a
         left join purchases b on a.id = b.customer_id
where a.id = 5;


select a.name, b.dateOfPurchase, c.description, b.quantityOfDrugs
from customer a
         inner join purchases b on a.id = b.customer_id
         inner join drug c on b.nomenklatura_id = c.id
where a.id = 5;


select *
from drug a
         left join purchases z on a.id = z.nomenklatura_id;


alter table customer
    add login char(5);


select purchases.id              as id,
       purchases.dateOfPurchase  as DOO,
       purchases.quantityOfDrugs as QOD,
       purchases.nomenklatura_id as nomenkla,
       purchases.customer_id     as Cust
from purchases purchases
         left outer join drug drug on purchases.nomenklatura_id = drug.id
where drug.id = 1