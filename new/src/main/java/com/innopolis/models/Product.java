package com.innopolis.models;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Product {
    private Integer id;
    private String brand;
    private String typeClothing;
    private String gender;
    private Double price;
    private Integer count;
}
