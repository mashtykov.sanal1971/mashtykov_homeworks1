package com.innopolis.control;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import com.innopolis.models.Product;
import com.innopolis.repositories.ProductRepository;

@Controller
public class ProductControl {

    @Autowired
    private ProductRepository productRepository;

    @PostMapping(value = "/products")
    public String addProduct(@RequestParam("brand") String brand,
                             @RequestParam("typeClothing") String typeClothing,
                             @RequestParam("gender") String gender,
                             @RequestParam("price") Double price,
                             @RequestParam("count") Integer count) {

        Product product = Product.builder()
                .brand(brand)
                .typeClothing(typeClothing)
                .gender(gender)
                .price(price)
                .count(count)
                .build();

        productRepository.save(product);
        return "redirect:/product.html";
    }
}
