CREATE TABLE product
(
    id serial PRIMARY KEY,
    description VARCHAR(100),
    price  DECIMAL(10,2),
    number INTEGER
);

CREATE TABLE customer
(
    id serial PRIMARY KEY,
    name VARCHAR(50)
);

CREATE TABLE orders
(
    product_id INTEGER,
    customer_id INTEGER,
    order_date DATE,
    number_of_products INTEGER,
    FOREIGN KEY (product_id) references product (id),
    FOREIGN KEY (customer_id) references customer (id)
);
insert into product(description, price, number) values ('Говядина', '300', '1200');
insert into product(description, price, number) values ('Рыба', '120', '800');
insert into product(description, price, number) values ('Баранина', '250', '700');
insert into product(description, price, number) values ('Свинина', '400', '900');

insert into customer(name) values ('Бадмаев М.П.');
insert into customer(name) values ('Демьянов С.Д.');
insert into customer(name) values ('Иван Иванов');
insert into customer(name) values ('Сангаджиев П.П.');
insert into customer(name) values ('Григорьев В.Н.');

insert into orders(product_id, customer_id, order_date, number_of_products) values ('1', '3', current_date, '10');
update product set number = number - 10 where id = 1;

insert into orders(product_id, customer_id, order_date, number_of_products) values ('4', '3', current_date, '5');
update product set number = number - 5 where id = 2;

insert into orders(product_id, customer_id, order_date, number_of_products) values ('1', '5', current_date, '15');
update product set number = number - 15 where id = 1;

insert into orders(product_id, customer_id, order_date, number_of_products) values ('1', '3', current_date, '10');
update product set number = number - 10 where id = 1;
